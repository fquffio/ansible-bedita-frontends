BEdita frontends
================

This role's duty is to set up multiple [BEdita](http://www.bedita.com) frontends.

Variables
---------

### `bedita_frontends`
A list of dictionaries of setup configuration for each frontend. Each dictionary has:

- a **required** key `key`, to uniquely identify the frontend
- a **required** key `repository`, to specify the Git repository to clone
- an *optional* key `version`, to tell which branch/tag/commit should be cloned (by default, `HEAD` is used)
- an *optional* key `root`, the directory where the project should be cloned (by default, `{{ bedita_root }}/frontends` is used — please, consider **not using** this setting!)
- a **required** key `url`, the main URL to the frontend
- an *optional* key `area_id` (default: `1`)
- an *optional* key `debug` (default: `{{ bedita_debug }}`)
- two *optional* keys `staging` and `draft` (default: `false`)


Example
-------

```yaml
---
bedita_frontends:
  - key: tessellate
    url: "http://tessellate.example.com"
    repository: https://github.com/fquffio/tessellate.git
    area_id: 42
    debug: 1
  - key: bootstrap
    url: "https://bootstrap.example.com"
    repository: https://github.com/bedita/bootstrap.git
    staging: true
    draft: true
```
